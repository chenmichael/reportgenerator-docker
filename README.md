# ReportGenerator Docker

Dockerized .NET ReportGenerator tool (https://github.com/danielpalme/ReportGenerator).

[![pipeline status](https://gitlab.com/chenmichael/reportgenerator-docker/badges/master/pipeline.svg)](https://gitlab.com/chenmichael/reportgenerator-docker/-/commits/master)
